import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CrudService } from '../service/crud.service';
import { FiltersService } from '../service/filters.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  Rozmiar: String[] = [];
  Siersc: String[] = [];
  Charakter: String[] = [];
  Umaszczenie: String[] = [];

  selectedRozmiar: {id: number, selected: boolean}[] = [];
  selectedSiersc: {id: number, selected: boolean}[] = [];
  selectedCharakter: {id: number, selected: boolean}[]  = [];
  selectedUmaszczenie: {id: number, selected: boolean}[]  = [];

  selectedCount: number;
  selected: boolean[] = [];
  
  RasyRozmiar: {rasa: any, id: any}[] = [];
  RasySiersc: {rasa: any, id: any}[] = [];
  RasyCharakter: {rasa: any, id: any}[] = [];
  RasyUmaszczenie: {rasa: any, id: any}[]= [];

  filteredRozmiar: any[] = [];
  filteredSiersc: any[] = [];
  filteredCharakter: any[] = [];
  filteredUmaszczenie: any[] = [];
  filteredRasy: any[] = [];
  
  @Output() data: EventEmitter<String[]> = new EventEmitter<String[]>();

  constructor( 
    private crudService: CrudService,
    private filtersService: FiltersService) { }
 

  ngOnInit(): void {
    this.crudService.GetAllRozmiar().subscribe(res => {
      this.Rozmiar= res;
      for(let i=0; i<res.length; i++) {
        this.selectedRozmiar.push({id: res[i][0].value, selected: false});
      }
    });
    this.crudService.GetAllSiersc().subscribe(res => {
      this.Siersc= res;
      for(let i=0; i<res.length; i++) {
        this.selectedSiersc.push({id: res[i][0].value, selected: false});
      }
    });
    this.crudService.GetAllCharakter().subscribe(res => {
      this.Charakter= res;
      for(let i=0; i<res.length; i++) {
        this.selectedCharakter.push({id: res[i][0].value, selected: false});
      }
    });
    this.crudService.GetAllUmaszczenie().subscribe(res => {
      this.Umaszczenie= res;
      for(let i=0; i<res.length; i++) {
        this.selectedUmaszczenie.push({id: res[i][0].value, selected: false});
      }
    });
    this.crudService.GetFilterUmaszczenie().subscribe(res => {
      for(let i = 0; i< res.length; i++) 
        this.RasyUmaszczenie.push({rasa: res[i][1].value, id: res[i][0].value});
    });
    this.crudService.GetFilterSiersc().subscribe(res => {
      for(let i = 0; i< res.length; i++) 
        this.RasySiersc.push({rasa: res[i][1].value, id: res[i][0].value});
    });
    this.crudService.GetFilterCharakter().subscribe(res => {
      for(let i = 0; i< res.length; i++) 
        this.RasyCharakter.push({rasa: res[i][1].value, id: res[i][0].value});
    });
    this.crudService.GetFilterRozmiar().subscribe(res => {
      for(let i = 0; i< res.length; i++) 
        this.RasyRozmiar.push({rasa: res[i][1].value, id: res[i][0].value});
    });

  }


  select(type: String, id: number): void {
    switch(type) {
      case 'rozmiar': {
        let index = this.selectedRozmiar.findIndex( x => x.id == id);
        this.selectedRozmiar[index].selected = !this.selectedRozmiar[index].selected;
        break;
      }
      case 'siersc': {
        let index = this.selectedSiersc.findIndex( x => x.id == id);
        this.selectedSiersc[index].selected = !this.selectedSiersc[index].selected;
        break;
      }
      case 'charakter': {
        let index = this.selectedCharakter.findIndex( x => x.id == id);
        this.selectedCharakter[index].selected = !this.selectedCharakter[index].selected;
        break;
      }
      case 'umaszczenie': {
        let index = this.selectedUmaszczenie.findIndex( x => x.id == id);
        this.selectedUmaszczenie[index].selected = !this.selectedUmaszczenie[index].selected;
        break;
      }      
    }
  }
  
  selectClass(type: String, id: number): String {
    switch(type) {
      case 'rozmiar': 
        return (this.selectedRozmiar[this.selectedRozmiar.findIndex( x => x.id == id)].selected == true) ? 'selected' : 'unselected';
      
      case 'siersc': 
        return (this.selectedSiersc[this.selectedSiersc.findIndex( x => x.id == id)].selected == true) ? 'selected' : 'unselected';
      
      case 'charakter': 
        return (this.selectedCharakter[this.selectedCharakter.findIndex( x => x.id == id)].selected) ? 'selected' : 'unselected';
      
      case 'umaszczenie': 
        return (this.selectedUmaszczenie[this.selectedUmaszczenie.findIndex( x => x.id == id)].selected)  ? 'selected' : 'unselected';
        
    }
  }

  clear(): void{
    this.selectedRozmiar.forEach(x => x.selected = false);
    this.selectedCharakter.forEach(x => x.selected = false);
    this.selectedUmaszczenie.forEach(x => x.selected = false);
    this.selectedSiersc.forEach(x => x.selected = false);
    this.filteredRasy = [];
    this.filteredRasy.push(true);
    this.data.emit(this.filteredRasy);
  }

  filter(): void {

    this.selectedCount = 0;
    this.selected = [];

    let rozmiar = this.filtersService.findInArray(this.selectedRozmiar, this.RasyRozmiar)
    this.selectedCount =+ rozmiar.selectedCount;
    this.selected['rozmiar'] = rozmiar.selected;
    this.filteredRozmiar = rozmiar.filtered;

    let charakter = this.filtersService.findInArray(this.selectedCharakter, this.RasyCharakter)
    this.selectedCount =+ charakter.selectedCount;
    this.selected['charakter'] = charakter.selected;
    this.filteredCharakter = charakter.filtered;

    let umaszczenie = this.filtersService.findInArray(this.selectedUmaszczenie, this.RasyUmaszczenie)
    this.selectedCount =+ umaszczenie.selectedCount;
    this.selected['umaszczenie'] = umaszczenie.selected;
    this.filteredUmaszczenie = umaszczenie.filtered;

    let siersc = this.filtersService.findInArray(this.selectedSiersc, this.RasySiersc)
    this.selectedCount =+ siersc.selectedCount;
    this.selected['siersc'] = siersc.selected;
    this.filteredSiersc = siersc.filtered;

    this.filteredRasy = [];

    this.filteredRasy = this.filtersService.andFilters(this.filteredRasy, this.filteredRozmiar, this.selected['rozmiar']);
    this.filteredRasy = this.filtersService.andFilters(this.filteredRasy, this.filteredCharakter, this.selected['charakter']);
    this.filteredRasy = this.filtersService.andFilters(this.filteredRasy, this.filteredUmaszczenie, this.selected['umaszczenie']);
    this.filteredRasy = this.filtersService.andFilters(this.filteredRasy, this.filteredSiersc, this.selected['siersc']);

    if(this.filteredRasy.length == 0) {
      if(this.selectedCount==0)
        this.filteredRasy.push(true);
      else
        this.filteredRasy.push(false);
    }
    this.data.emit(this.filteredRasy);
    
  }
}
