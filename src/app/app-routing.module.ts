import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllBreedsComponent } from './all-breeds/all-breeds.component';
import {DogDetailComponent} from './dog-detail/dog-detail.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'breeds'},
  { path: 'breeds', component: AllBreedsComponent},
  { path: 'breeds/:id', component: DogDetailComponent }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
