import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { CrudService } from '../service/crud.service';


@Component({
  selector: 'app-all-breeds',
  templateUrl: './all-breeds.component.html',
  styleUrls: ['./all-breeds.component.scss']
})
export class AllBreedsComponent implements OnInit {


  Rasy:any = [];
  Zdjecia: any = [];
  Alfabet:  any = [{letter: 'A', state: false},{letter: 'B', state: false}, {letter: 'C', state: false},
                  {letter: 'D', state: false},{letter: 'E', state: false}, {letter: 'F', state: false},
                  {letter: 'G', state: false},{letter: 'H', state: false}, {letter: 'I', state: false},
                  {letter: 'J', state: false},{letter: 'K', state: false}, {letter: 'L', state: false},
                  {letter: 'M', state: false},{letter: 'N', state: false}, {letter: 'O', state: false},
                  {letter: 'P', state: false},{letter: 'Q', state: false}, {letter: 'R', state: false},
                  {letter: 'S', state: false},{letter: 'T', state: false}, {letter: 'U', state: false},
                  {letter: 'V', state: false},{letter: 'W', state: false}, {letter: 'X', state: false},
                  {letter: 'Y', state: false},{letter: 'Z', state: false}, {letter: 'WSZYSTKIE RASY', state: true}];               
  displayedRasy: any = [];
  filteredRasy: any = [];
  searchedRasy: any = [];
  filterInfo: String;
  selectedLetter: String;
  searchName: String;

  constructor(
    private crudService: CrudService,
    private sanitizer: DomSanitizer
    ) {}

  ngOnInit(): void {
    this.crudService.GetRasy().subscribe(res => {
      this.Rasy = res;
      this.displayedRasy = res;
      this.searchedRasy = res;
      this.filteredRasy = res;
      for (let i = 0; i < res.length; i++) {
        let letter = res[i][0].value.substring(0,1).toUpperCase();
        let index = this.Alfabet.findIndex(x => x.letter == letter);
        this.Alfabet[index].state = true;
        
      }
    });
    
    this.crudService.GetZdjecia().subscribe(res => {
      this.Zdjecia = res;
      for(var i=0; i<this.Zdjecia.length; i++)
      {
        var binary = '';
        var bytes = new Uint8Array( this.Zdjecia[i][1].value.data);
	      var len = bytes.byteLength;
	      for (var j = 0; j < len; j++) {
		      binary += String.fromCharCode( bytes[ j ] );
	      }
        var btoaVal = btoa(binary);
        this.Zdjecia[i][1].value = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'+ btoaVal);
      }
      
    })  
    
    this.selectedLetter = 'WSZYSTKIE RASY';
  }

  search(event: any) {
    if(this.searchedRasy.length > 0) {
      this.filterInfo = '';
      let filtered = this.searchedRasy;
      if (event.target.value) {
        let text = event.target.value.toLowerCase();
        filtered = filtered.filter(i => i[0].value.toLowerCase().includes(text));
      }
      if(filtered.length == 0){
        this.filterInfo = 'Nie znaleziono takiej rasy. Przepraszamy.';
      }
      this.displayedRasy = filtered;
    }
  }

  alphabetSearch(letter: String) {
    this.filterInfo = '';
    this.selectedLetter = letter;
    this.searchName = '';
    if(letter == 'WSZYSTKIE RASY') {
      this.displayedRasy = this.filteredRasy;
      this.searchedRasy = this.filteredRasy;
    }
    else {
      let filtered = this.filteredRasy;
      filtered = filtered.filter(i => i[0].value.toUpperCase().substring(0,1).includes(letter));
      this.displayedRasy = filtered;
      this.searchedRasy = filtered;
      
      if(filtered == 0)
        this.filterInfo = 'Brak ras na wybraną literę. Przepraszamy.'
    }
  }

  eventHandler(event: any[]){
    this.filterInfo = '';
    this.searchName = '';
    if(event[0] === true)
      this.filteredRasy = this.Rasy;
    else if (event[0] === false) {
      this.filterInfo = 'Brak ras o wybranych cechach. Przepraszamy.';
      this.filteredRasy = [];
    }      
    else {
      let filtered = [];
      for( let i = 0; i < this.Rasy.length; i++) {
        if(event.includes(this.Rasy[i][1].value)){
          filtered.push(this.Rasy[i]);
        }
      }
      this.filteredRasy = filtered;
    }
    this.displayedRasy = this.filteredRasy;
  }
 
}
