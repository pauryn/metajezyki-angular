import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FiltersService {

  constructor() { }

  findDuplicates(arr: any[]) : any{
    let seen = [];
    let a = [];
    arr.forEach( function (str) {
      if (seen[str])
        {
          a.push(str);
          }
      else
        seen[str] = true;
    });
    return a;
  }

  getAllId(arr: any[], val: any) :any[]{
    let indexes = [];
    for(let i = 0; i < arr.length; i++) {
      if (arr[i].id == val)
        indexes.push(arr[i].rasa);
    }
    return indexes;
  }

  findInArray(selectedArr: any[], arr: any[]){
    let data = {selectedCount: 0, selected: false, filtered: []}
    let array = [];
    let number = 0;

    for(let i=0; i<selectedArr.length; i++) {
      if(selectedArr[i].selected==true) {
        data.selectedCount++;
        data.selected = true;
        let ids = this.getAllId(arr, selectedArr[i].id);
        ids.forEach(function (x) {
          array.push(x);
        });
        if(number>0) {
          data.filtered = this.findDuplicates(array);
          array = data.filtered
        }
        else
          data.filtered = array;        
        number++;
      }
    }
    return data;

  }

  andFilters(rasy: any[], filtered: any[], selected: boolean) {
    if(rasy[0] != false) {
      if (filtered.length > 0) {
        if (rasy.length > 0) {
          let a = filtered.concat(rasy)
          rasy = this.findDuplicates(a);
          if(rasy.length==0)
            rasy.push(false);
        }
        else rasy = filtered;
      }
      else if (selected == true) {
          rasy = [];
          rasy.push(false);
      }
    }
    return rasy;
  }
}
