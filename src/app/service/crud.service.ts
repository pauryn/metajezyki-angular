import { Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class CrudService {

  // Node/Express API
  REST_API: string = 'https://afternoon-everglades-89596.herokuapp.com/api';

  // Http Header
  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) { }

  GetRasy() : Observable<any>{
    return this.httpClient.get(`${this.REST_API}/all-breeds`);
  }

  GetOneRasy(id:any) : Observable<any>{
    let API_URL = `${this.REST_API}/dog-details/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(map((res: any) => {
          return res || {}
        }),
        catchError(this.handleError)
      )
  }

  GetZdjecia() : Observable<any>{
    return this.httpClient.get(`${this.REST_API}/photos`);
  }

  GetOneZdjecia(id:any): Observable<any> {
    let API_URL = `${this.REST_API}/photos/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(map((res: any) => {
        
          return res || {}
        }),
        catchError(this.handleError)
      )
  }

  GetOneRozmiar(id:any): Observable<any> {
    let API_URL = `${this.REST_API}/dog-details/size/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(map((res: any) => {
          return res || {}
        }),
        catchError(this.handleError)
      )
  }

  GetAllRozmiar(): Observable<any>{
    return this.httpClient.get(`${this.REST_API}/size`);
  }

  GetOneSiersc(id:any): Observable<any> {
    let API_URL = `${this.REST_API}/dog-details/fur/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(map((res: any) => {
          return res || {}
        }),
        catchError(this.handleError)
      )
  }

  GetAllSiersc(): Observable<any> {
    return this.httpClient.get(`${this.REST_API}/fur `);
  }

  GetOneCharakter(id:any): Observable<any> {
    let API_URL = `${this.REST_API}/dog-details/character/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(map((res: any) => {
          return res || {}
        }),
        catchError(this.handleError)
      )
  }

  GetAllCharakter(): Observable<any> {
    return this.httpClient.get(`${this.REST_API}/character`);
  }

  GetOneUmaszczenie(id:any): Observable<any> {
    let API_URL = `${this.REST_API}/dog-details/color/${id}`;
    return this.httpClient.get(API_URL, { headers: this.httpHeaders })
      .pipe(map((res: any) => {
          return res || {}
        }),
        catchError(this.handleError)
      )
  }

  GetAllUmaszczenie(): Observable<any> {
    return this.httpClient.get(`${this.REST_API}/color`);
  }

  GetFilterRozmiar(): Observable<any> {
    return this.httpClient.get(`${this.REST_API}/filter/size`);
  }

  GetFilterCharakter(): Observable<any> {
    return this.httpClient.get(`${this.REST_API}/filter/character`);
  }

  GetFilterUmaszczenie(): Observable<any> {
    return this.httpClient.get(`${this.REST_API}/filter/color`);
  }

  GetFilterSiersc(): Observable<any> {
    return this.httpClient.get(`${this.REST_API}/filter/fur`);
  }

  // Error 
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
