import { ViewportScroller } from '@angular/common';
import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute} from '@angular/router';
import { CrudService } from '../service/crud.service';

@Component({
  selector: 'app-dog-detail',
  templateUrl: './dog-detail.component.html',
  styleUrls: ['./dog-detail.component.scss']
})
export class DogDetailComponent implements OnInit {
  
  getId: any;
  Rasy: any[];
  Zdjecia: any[];
  Rozmiar: any[];
  Siersc: any[];
  Charakter: any[];
  Umaszczenie: any[];

  active = '1';
  areas = ['1', '2', '3', '4'];

  @ViewChild('photos')
  photos: ElementRef;
  @ViewChild('info')
  info: ElementRef;
  @ViewChild('appearance')
  appearance: ElementRef;
  @ViewChild('character')
  character: ElementRef;

  constructor(
    private activatedRoute: ActivatedRoute,
    private crudService: CrudService,
    private sanitizer: DomSanitizer,
  ){}

  ngOnInit(): void {
    this.getId = this.activatedRoute.snapshot.paramMap.get('id');
    this.crudService.GetOneRasy(this.getId).subscribe(res => {
      this.Rasy = res;
    });
    this.crudService.GetOneZdjecia(this.getId).subscribe(res => {
      this.Zdjecia= res;
      for(var i=0; i<this.Zdjecia.length; i++)
      {
        var binary = '';
        var bytes = new Uint8Array( this.Zdjecia[i][1].value.data);
	      var len = bytes.byteLength;
	      for (var j = 0; j < len; j++) {
		      binary += String.fromCharCode( bytes[ j ] );
	      }
        var btoaVal = btoa(binary);
        this.Zdjecia[i][1].value = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'+ btoaVal);
      }
      
    });
    this.crudService.GetOneRozmiar(this.getId).subscribe(res => {
      this.Rozmiar= res;
    });
    this.crudService.GetOneSiersc(this.getId).subscribe(res => {
      this.Siersc= res;
    });
    this.crudService.GetOneCharakter(this.getId).subscribe(res => {
      this.Charakter= res;
    });
    this.crudService.GetOneUmaszczenie(this.getId).subscribe(res => {
      this.Umaszczenie= res;
    });

    
  }

  scrollPhotos(){
    this.photos.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'end'})
  }
  scrollInfo(){
    this.info.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' })
  }
  scrollAppearance(){
    this.appearance.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' })
  }
  scrollCharacter(){
    this.character.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start'})
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
  this.active = this.areas[
      Math.round(window.pageYOffset/ document.documentElement.clientHeight)
  ];
}

}